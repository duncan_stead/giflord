# README #

### What is this repository for? ###

Gif Lord is a simple tool for game developers to upload gifs or images to multiple social media accounts in one click.
A message can be added to each post, and any gif will be rescaled for Twitter to reduce compression artifacts.


![Example of using the tool](Example.gif)


### How do I get set up? ###

If you just want to run it, the latest build can be found **[here](https://bitbucket.org/duncan_stead/giflord/downloads/GifLord.zip)**

##### Summary of set up
The solution file is for Visual Studio 2017, and includes some Nuget packages that it should pull in automatically.

##### Dependencies
TweetinviAPI via Nuget.
Gifsicle via directly included copy (then moved to the output directory with a build script)

##### Deployment instructions
To use Discord, you will need a webhook set up on your server.
To use Twitter, you will need a Developer Account to get the necessary keys.

To make it easier to use, I recommend putting a shortcut to the Gif Lord exe inside your gif/image folder, so you can drag files onto the shortcut to open them directly.

### Contribution guidelines ###

This is a very basic project, but I'd welcome any pull requests or feedback.
I think it could be interesting to support other social media endpoints.

### Who do I talk to? ###

The owner of this repo is Duncan Stead.