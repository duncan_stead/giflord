﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GifLord
{
    [System.Serializable]
    public class DiscordConfig
    {
        public string m_webhookURL;

        public static string s_configPath = "DiscordConfig.json";
    }
    class Discord
    {
        public DiscordConfig m_config;

        public Discord()
        {
            m_config = ConfigLoader.LoadConfig<DiscordConfig>(DiscordConfig.s_configPath);
        }
        public async Task<bool> SendMessage(string message, string gifPath)
        {
            if (m_config.m_webhookURL == null || m_config.m_webhookURL.Length == 0)
            {
                MessageBox.Show("Error, discord webhook not configured!");
                return false;
            }
            string filename = Path.GetFileNameWithoutExtension(gifPath);
            HttpClient client = new HttpClient();

            byte[] bytes = File.ReadAllBytes(gifPath);

            var content = new MultipartFormDataContent();
            content.Add(new StringContent(message), "content");
            content.Add(new ByteArrayContent(bytes, 0, bytes.Length), "file", "Gif.gif");

            var response = await client.PostAsync(m_config.m_webhookURL, content);
            await response.Content.ReadAsStringAsync();
            return true;
        }
    }
}
