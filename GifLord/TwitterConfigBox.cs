﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GifLord
{
    public partial class TwitterConfigBox : Form
    {
        public TwitterConfigBox()
        {
            InitializeComponent();
        }
        TwitterConfig m_config;
        public void SetConfig(TwitterConfig config)
        {
            m_config = config;
            textAPIKey.Text = config.m_consumerAPIKey;
            textAPISecretKey.Text = config.m_consumerAPISecretKey;
            textAccessToken.Text = config.m_accessToken;
            textAccessTokenSecret.Text = config.m_accessTokenSecret;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_config.m_consumerAPIKey = textAPIKey.Text;
            m_config.m_consumerAPISecretKey = textAPISecretKey.Text;
            m_config.m_accessToken = textAccessToken.Text;
            m_config.m_accessTokenSecret = textAccessTokenSecret.Text;
            ConfigLoader.WriteConfig<TwitterConfig>(TwitterConfig.s_configPath, m_config);

            this.Close();
        }
    }
}
