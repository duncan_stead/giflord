﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GifLord
{
    [System.Serializable]
    class MessageInfo
    {
        public string FilePath
        {
            get; set;
        }
        public string Message
        {
            get; set;
        }
    }
}
