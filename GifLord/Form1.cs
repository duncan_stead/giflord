﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Parameters;

namespace GifLord
{
    public partial class Form1 : Form
    {
        const string c_twitterTemplateFile = "TwitterTemplate.txt";
        BindingList<MessageInfo> m_messages;

        Discord m_discord;
        Twitter m_twitter;

        public Form1()
        {
            InitializeComponent();
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(Form1_DragEnter);
            this.DragDrop += new DragEventHandler(Form1_DragDrop);
            m_pictureBoxStartWidth = pictureBox1.Width;
            m_pictureBoxStartHeight = pictureBox1.Height;

            if (File.Exists(c_twitterTemplateFile))
            {
                string templateText = File.ReadAllText(c_twitterTemplateFile);
                textBox1.Text = templateText;
            }

            m_messages = new BindingList<MessageInfo>();
            m_messages.AllowRemove = true;
            m_messages.AllowEdit = true;
            m_messages.AllowNew = false;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridView1.DataSource = m_messages;
            dataGridView1.Columns[0].ReadOnly = true;
            dataGridView1.AllowUserToDeleteRows = true;

            timeBeginPeriod(timerAccuracy);
            InitializeRedrawTimer();

            m_discord = new Discord();
            m_twitter = new Twitter();
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            timeEndPeriod(timerAccuracy);
            base.OnFormClosed(e);
        }

        // Pinvoke:
        private const int timerAccuracy = 10;
        [System.Runtime.InteropServices.DllImport("winmm.dll")]
        private static extern int timeBeginPeriod(int msec);
        [System.Runtime.InteropServices.DllImport("winmm.dll")]
        public static extern int timeEndPeriod(int msec);

        /*void ChangeDisplayedImage(string imagePath)
        {
            pictureBox1.ImageLocation = imagePath;
        }*/

        private int m_pictureBoxStartWidth;
        private int m_pictureBoxStartHeight;
        private FrameDimension dimension;
        private int frameCount;
        private int indexToPaint;
        private Timer timer = new Timer();
        private float m_zoomFactor;
        private string m_currentlyDiplayedImage;

        void SetButtonsEnabled(bool enabled)
        {
            Submit.Enabled = enabled;
            DiscordSubmit.Enabled = enabled;
            DiscordConfig.Enabled = enabled;
            TwitterSubmit.Enabled = enabled;
            TwitterConfig.Enabled = enabled;
            this.Refresh();
        }

        private void InitializeRedrawTimer()
        {
            this.pictureBox1.Paint += new PaintEventHandler(pictureBox1_Paint);
            
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            CheckForChangingImage();
            if (frameCount == 0)
            {
                return;
            }
            indexToPaint++;
            if (indexToPaint >= frameCount)
            {
                indexToPaint = 0;
            }
        }
        void CheckForChangingImage()
        {
            var selected = dataGridView1.SelectedCells;
            if (selected.Count == 0)
            {
                return;
            }
            int rowIndex = selected[0].RowIndex;
            if (rowIndex >= 0)
            {
                string newPath = m_messages[rowIndex].FilePath;
                if (newPath != m_currentlyDiplayedImage)
                {
                    m_currentlyDiplayedImage = newPath;
                    ActuallySwitchImage(newPath);
                }
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }
        void ActuallySwitchImage(string imagePath)
        {
            if (pictureBox1.Image != null)
            {
                pictureBox1.Image.Dispose();
                pictureBox1.Image = null;
            }
            Image loaded = Image.FromFile(imagePath);
            pictureBox1.Image = loaded;
            try
            {
                PropertyItem item = loaded.GetPropertyItem(0x5100); // FrameDelay in libgdiplus
                                                                    // Time is in milliseconds
                timer.Interval = (item.Value[0] + item.Value[1] * 256) * 10;
                m_zoomFactor = Math.Min((float)m_pictureBoxStartWidth / (float)loaded.Width, (float)m_pictureBoxStartHeight / (float)loaded.Height);
                float pictureWidth = loaded.Width * m_zoomFactor;
                float pictureHeight = loaded.Height * m_zoomFactor;
                pictureBox1.Width = (int)pictureWidth;
                pictureBox1.Height = (int)pictureHeight;

                dimension = new FrameDimension(this.pictureBox1.Image.FrameDimensionsList[0]);
                frameCount = this.pictureBox1.Image.GetFrameCount(dimension);
                indexToPaint = 0;
            }
            catch(Exception e)
            {
                // Might be a non gif image
                frameCount = 0;
                pictureBox1.Width = m_pictureBoxStartWidth;
                pictureBox1.Height = m_pictureBoxStartHeight;
            }
        }

        void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (frameCount == 0)
            {
                return;
            }
            this.pictureBox1.Image.SelectActiveFrame(dimension, indexToPaint);
            e.Graphics.ScaleTransform(m_zoomFactor, m_zoomFactor);
            e.Graphics.DrawImage(this.pictureBox1.Image, Point.Empty);
        }

        void LoadTwitterTemplate()
        {
            if (File.Exists(c_twitterTemplateFile))
            {
                string templateText = File.ReadAllText(c_twitterTemplateFile);
                textBox1.Text = templateText;
            }
        }
        void SaveTwitterTemplate(string text)
        {
            File.WriteAllText(c_twitterTemplateFile, text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            SaveTwitterTemplate(textBox1.Text);
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            AddFiles(files);
        }
        public void AddFiles(string[] files)
        {
            foreach (string file in files)
            {
                bool alreadyInList = false;
                foreach(MessageInfo m in m_messages)
                {
                    if (m.FilePath == file)
                    {
                        alreadyInList = true;
                        break;
                    }
                }
                if (alreadyInList)
                {
                    continue;
                }
                MessageInfo message = new MessageInfo();
                message.FilePath = file;
                message.Message = "";
                m_messages.Add(message);
            }
            dataGridView1.Refresh();
        }

        

        private void Submit_Click(object sender, EventArgs e)
        {
            DoSubmission(true, true);
        }
        private void DiscordSubmit_Click(object sender, EventArgs e)
        {
            DoSubmission(true, false);
        }
        private void TwitterSubmit_Click(object sender, EventArgs e)
        {
            DoSubmission(false, true);
        }
        async void DoSubmission(bool sendDiscord, bool sendTwitter)
        {
            SetButtonsEnabled(false);
            foreach (MessageInfo message in m_messages)
            {
                bool success = await BroadcastMessage(message, sendDiscord, sendTwitter);
                if (!success)
                {
                    MessageBox.Show("Submission of " + message.FilePath +" skipped due to error!");
                    return;
                }
            }
            SetButtonsEnabled(true);

            var form = new CompletionPopup();
            ShowDialogCentered(form);
        }
        public void ShowDialogCentered(Form form)
        {
            form.StartPosition = FormStartPosition.Manual;
            form.Location = new Point(Location.X + (Width / 2) - (form.Width / 2), Location.Y + (Height / 2) - (form.Height / 2));
            form.ShowDialog();
        }

        async Task<bool> BroadcastMessage(MessageInfo info, bool sendDiscord = true, bool sendTwitter = true)
        {
            string message = info.Message;
            string gifPath = info.FilePath;

            if (sendTwitter)
            {
                string twitterTemplate = textBox1.Text;
                bool success = await m_twitter.SendMessage(message, gifPath, twitterTemplate);
                if (!success)
                {
                    return false;
                }
            }

            if (sendDiscord)
            {
                bool success = await m_discord.SendMessage(message, gifPath);
                if (!success)
                {
                    return false;
                }
            }
            return true;
        }

        private void DiscordConfig_Click(object sender, EventArgs e)
        {
            var form = new DiscordConfigBox();
            form.SetConfig(m_discord.m_config);
            ShowDialogCentered(form);
        }

        private void TwitterConfig_Click(object sender, EventArgs e)
        {
            var form = new TwitterConfigBox();
            form.SetConfig(m_twitter.m_config);
            ShowDialogCentered(form);
        }
    }
}
