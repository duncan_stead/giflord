﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GifLord
{
    public partial class DiscordConfigBox : Form
    {
        public DiscordConfigBox()
        {
            InitializeComponent();
        }

        DiscordConfig m_config;
        public void SetConfig(DiscordConfig config)
        {
            m_config = config;

            textBox1.Text = config.m_webhookURL;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            m_config.m_webhookURL = textBox1.Text;
            ConfigLoader.WriteConfig<DiscordConfig>(DiscordConfig.s_configPath, m_config);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
