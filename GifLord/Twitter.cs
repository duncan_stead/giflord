﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Parameters;

namespace GifLord
{
    [System.Serializable]
    public class TwitterConfig
    {
        public string m_consumerAPIKey;
        public string m_consumerAPISecretKey;
        public string m_accessToken;
        public string m_accessTokenSecret;

        public static string s_configPath = "TwitterConfig.json";
    }
    class Twitter
    {
        public TwitterConfig m_config;

        public Twitter()
        {
            m_config = ConfigLoader.LoadConfig<TwitterConfig>(TwitterConfig.s_configPath);
        }

        bool UpscaleGif(string gifPath, string twitterGifPath, int scale, out long outputSize, out int width, out int height)
        {
            outputSize = 0;
            width = 0;
            height = 0;
            int exitCode = 0;
            try
            {
                string command = "--scale " + scale + " \"" + gifPath + "\" -o " + twitterGifPath;
                using (Process process = Process.Start("gifsicle", command))
                {
                    process.WaitForExit();
                    exitCode = process.ExitCode;
                }
            }
            catch
            {
                MessageBox.Show("Error with gif scaling process for twitter!");
                return false;
            }
            if (exitCode != 0)
            {
                MessageBox.Show("Error with gif scaling process for twitter!");
                return false;
            }
            FileInfo fi = new FileInfo(twitterGifPath);
            outputSize = fi.Length;
            System.Drawing.Image img = System.Drawing.Image.FromFile(twitterGifPath);
            //MessageBox.Show("Width: " + img.Width + ", Height: " + img.Height);
            width = img.Width;
            height = img.Height;
            img.Dispose();
            return true;
        }

        public async Task<bool> SendMessage(string message, string gifPath, string twitterTemplate)
        {
            string messagePath = gifPath;
            if (Path.GetExtension(gifPath) == ".gif")
            {
                string twitterGifPath = "TwitterRescale.gif";
                messagePath = twitterGifPath;
                long scaledGifSize = 0;
                long gifSizeLimit = 5 * 1024 * 1024;
                int gifWidthLimit = 1280;
                int gifHeightLimit = 1024;
                for (int scale = 4; scale >= 1; scale--)
                {
                    int width;
                    int height;
                    bool worked = UpscaleGif(gifPath, twitterGifPath, scale, out scaledGifSize, out width, out height);
                    if (!worked)
                    {
                        return false;
                    }
                    if (width < 32 || height < 32)
                    {
                        MessageBox.Show("Width and height must be at least 32x32");
                        return false;
                    }
                    if (scaledGifSize < gifSizeLimit && width <= gifWidthLimit && height <= gifHeightLimit)
                    {
                        break;
                    }
                }
            }
            TwitterConfig config = m_config;
            TwitterClient t = new TwitterClient(
                config.m_consumerAPIKey, config.m_consumerAPISecretKey,
                config.m_accessToken, config.m_accessTokenSecret);
            byte[] gifBytes = File.ReadAllBytes(messagePath);

            var upload = await t.Upload.UploadBinaryAsync(gifBytes);

            string twitterMessage = string.Format(twitterTemplate, message);
            var tweet = await t.Tweets.PublishTweetAsync(new PublishTweetParameters(twitterMessage)
            {
                Medias = { upload }
            });
            return true;
        }
    }
}
