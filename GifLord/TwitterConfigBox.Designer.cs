﻿namespace GifLord
{
    partial class TwitterConfigBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textAPIKey = new System.Windows.Forms.TextBox();
            this.textAPISecretKey = new System.Windows.Forms.TextBox();
            this.textAccessToken = new System.Windows.Forms.TextBox();
            this.textAccessTokenSecret = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "API Key:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "API Secret Key:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Access Token:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Access Token Secret:";
            // 
            // textAPIKey
            // 
            this.textAPIKey.Location = new System.Drawing.Point(205, 13);
            this.textAPIKey.Name = "textAPIKey";
            this.textAPIKey.Size = new System.Drawing.Size(424, 20);
            this.textAPIKey.TabIndex = 4;
            // 
            // textAPISecretKey
            // 
            this.textAPISecretKey.Location = new System.Drawing.Point(205, 40);
            this.textAPISecretKey.Name = "textAPISecretKey";
            this.textAPISecretKey.Size = new System.Drawing.Size(424, 20);
            this.textAPISecretKey.TabIndex = 5;
            // 
            // textAccessToken
            // 
            this.textAccessToken.Location = new System.Drawing.Point(205, 67);
            this.textAccessToken.Name = "textAccessToken";
            this.textAccessToken.Size = new System.Drawing.Size(424, 20);
            this.textAccessToken.TabIndex = 6;
            // 
            // textAccessTokenSecret
            // 
            this.textAccessTokenSecret.Location = new System.Drawing.Point(205, 94);
            this.textAccessTokenSecret.Name = "textAccessTokenSecret";
            this.textAccessTokenSecret.Size = new System.Drawing.Size(424, 20);
            this.textAccessTokenSecret.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(271, 139);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TwitterConfigBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 174);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textAccessTokenSecret);
            this.Controls.Add(this.textAccessToken);
            this.Controls.Add(this.textAPISecretKey);
            this.Controls.Add(this.textAPIKey);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TwitterConfigBox";
            this.Text = "TwitterConfig";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textAPIKey;
        private System.Windows.Forms.TextBox textAPISecretKey;
        private System.Windows.Forms.TextBox textAccessToken;
        private System.Windows.Forms.TextBox textAccessTokenSecret;
        private System.Windows.Forms.Button button1;
    }
}