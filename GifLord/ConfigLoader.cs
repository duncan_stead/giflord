﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GifLord
{
    class ConfigLoader
    {
        public static T LoadConfig<T>(string filename) where T : class, new()
        {
            if (File.Exists(filename))
            {
                using (StreamReader r = new StreamReader(filename))
                {
                    string json = r.ReadToEnd();
                    return JsonConvert.DeserializeObject<T>(json);
                }
            }
            return new T();
        }
        public static bool WriteConfig<T>(string filename, T toWrite) where T : class
        {
            try
            {
                using (StreamWriter w = new StreamWriter(filename, false))
                {
                    string json = JsonConvert.SerializeObject(toWrite);
                    w.Write(json);
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception " + e.Message);
                return false;
            }
        }
    }
}
