﻿namespace GifLord
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Submit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.DiscordSubmit = new System.Windows.Forms.Button();
            this.TwitterSubmit = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TwitterConfig = new System.Windows.Forms.Button();
            this.DiscordConfig = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Submit
            // 
            this.Submit.Location = new System.Drawing.Point(29, 147);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(75, 23);
            this.Submit.TabIndex = 0;
            this.Submit.Text = "Submit";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Drag and drop to add files";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(29, 39);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(739, 97);
            this.dataGridView1.TabIndex = 5;
            // 
            // DiscordSubmit
            // 
            this.DiscordSubmit.Location = new System.Drawing.Point(261, 234);
            this.DiscordSubmit.Name = "DiscordSubmit";
            this.DiscordSubmit.Size = new System.Drawing.Size(75, 23);
            this.DiscordSubmit.TabIndex = 6;
            this.DiscordSubmit.Text = "Discord Only";
            this.DiscordSubmit.UseVisualStyleBackColor = true;
            this.DiscordSubmit.Click += new System.EventHandler(this.DiscordSubmit_Click);
            // 
            // TwitterSubmit
            // 
            this.TwitterSubmit.Location = new System.Drawing.Point(261, 147);
            this.TwitterSubmit.Name = "TwitterSubmit";
            this.TwitterSubmit.Size = new System.Drawing.Size(75, 23);
            this.TwitterSubmit.TabIndex = 7;
            this.TwitterSubmit.Text = "Twitter Only";
            this.TwitterSubmit.UseVisualStyleBackColor = true;
            this.TwitterSubmit.Click += new System.EventHandler(this.TwitterSubmit_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(384, 158);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(215, 41);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "{0} #indiedev";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(381, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Twitter Template:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(111, 142);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 115);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // TwitterConfig
            // 
            this.TwitterConfig.Location = new System.Drawing.Point(605, 147);
            this.TwitterConfig.Name = "TwitterConfig";
            this.TwitterConfig.Size = new System.Drawing.Size(82, 23);
            this.TwitterConfig.TabIndex = 11;
            this.TwitterConfig.Text = "Twitter Config";
            this.TwitterConfig.UseVisualStyleBackColor = true;
            this.TwitterConfig.Click += new System.EventHandler(this.TwitterConfig_Click);
            // 
            // DiscordConfig
            // 
            this.DiscordConfig.Location = new System.Drawing.Point(384, 234);
            this.DiscordConfig.Name = "DiscordConfig";
            this.DiscordConfig.Size = new System.Drawing.Size(90, 23);
            this.DiscordConfig.TabIndex = 12;
            this.DiscordConfig.Text = "Discord Config";
            this.DiscordConfig.UseVisualStyleBackColor = true;
            this.DiscordConfig.Click += new System.EventHandler(this.DiscordConfig_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 274);
            this.Controls.Add(this.DiscordConfig);
            this.Controls.Add(this.TwitterConfig);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.TwitterSubmit);
            this.Controls.Add(this.DiscordSubmit);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Submit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "GifLord";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button DiscordSubmit;
        private System.Windows.Forms.Button TwitterSubmit;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button TwitterConfig;
        private System.Windows.Forms.Button DiscordConfig;
    }
}

